using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Platformer2D
{
    public class Main : MonoBehaviour
    {
        [SerializeField] private int _charAnimationSpeed;
        [Header("Characters")]
        [SerializeField] private List<LevelObjectView>  _charViews;
        [SerializeField] private AnimState _animState;
        [SerializeField] private float _lvlAnimatiionSpeed;
        
        private List<LevelObjectView> _levelViews;
        private CharAnimatorController[]  _charsAnimator;
        private LevelBackgroundController[] _levelAnimator;

        private CameraController _cameraController;
        private PlayerController _playerController;
        

        void Start()
        {
            // Char controller init
            _charsAnimator = new CharAnimatorController[_charViews.Count];
            for(var i = 0; i < _charViews.Count; i++)
            {
                var config = Resources.Load<CharAnimatorConfig>( _charViews[i].tag + "AnimatorCfg");
                if (config)
                {
                    _charsAnimator[i] = new CharAnimatorController(config);
                    _playerController = new PlayerController(_charViews[i], _charsAnimator[i]);
                    if (_charViews[i].tag == "Player")
                    {
                        _cameraController = new CameraController(_charViews[i].transform, Camera.main.transform);
                    }
                }
            }

            // Level controller init
            _levelViews = FindObjectsOfType<LevelObjectView>()
                .ToList()
                .FindAll(x => x.gameObject.CompareTag("Level"))
                .OrderBy(x => x.name)
                .ToList();
            
            _levelAnimator = new LevelBackgroundController[_levelViews.Count];
            var lvlConfig = Resources.Load<LevelAnimatorConfig>( _levelViews[_levelViews.Count-1].tag + "AnimatorCfg");
            _lvlAnimatiionSpeed = lvlConfig.speed;
            for(var i = 0; i < _levelViews.Count; i++)
            {
                _levelViews[i].backgroundID = i;
                if (lvlConfig)
                {
                    _levelAnimator[i] = new LevelBackgroundController(lvlConfig, _levelViews[i]);
                }
            }

        }

        void Update()
        {
            _playerController.Update();
            
            for (int i = 0; i < _levelAnimator.Length; i++)
            {
                _levelViews[i].transform.position = _levelAnimator[i].Update(_lvlAnimatiionSpeed);
            }
            _cameraController.Update();
        }
    }
}
