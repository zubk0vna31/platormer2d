using UnityEngine;

namespace Platformer2D
{
    public class LevelObjectView : MonoBehaviour
    {
        public Transform transform;
        public SpriteRenderer spriteRenderer;
        public Collider2D collider;
        public Rigidbody2D rigidbody;
        [HideInInspector] public int backgroundID;
    }
}
