﻿using UnityEngine;

namespace Platformer2D
{
	public class LevelBackgroundController
	{
		private float _speed;
		private float _destinationPoint;
		private float _startPoint;
		private float _move;
		private Vector3 _transformPosition = Vector3.zero;
		private int _backgroundID;

		public LevelBackgroundController(LevelAnimatorConfig config, LevelObjectView levelObjectView)
		{
			_destinationPoint = config.destinationPoint;
			_startPoint = config.startPoint;
			_transformPosition = levelObjectView.transform.position;
			_speed = config.speed;
			_backgroundID = levelObjectView.backgroundID;
			SpeedSetup(_speed, _backgroundID);
		}

		

		public Vector3 Update(float lvlSpeed)
		{
			SpeedSetup(lvlSpeed,_backgroundID);
			_move = _transformPosition.x;
			_move += _speed * Time.fixedDeltaTime;
			_destinationPoint = _speed > 0 ? -Mathf.Abs(_destinationPoint) : Mathf.Abs(_destinationPoint);
			_startPoint = _speed > 0 ? -Mathf.Abs(_startPoint) : Mathf.Abs(_startPoint);
			
			if (Mathf.Abs(_move) >= Mathf.Abs(_destinationPoint))
			{
				_transformPosition.x = _startPoint + _speed * Time.fixedDeltaTime;
			}
			else
			{
				_transformPosition.x = _move;
			}

			return _transformPosition;
		}
		private void SpeedSetup(float speed, int backgroundID)
		{
			if (backgroundID == 0 || backgroundID == 1)
			{
				_speed = speed;
			}
			else if (backgroundID % 2 != 0)
			{
				_speed = speed / (backgroundID - 1);
			}
			else
			{
				_speed = speed / (backgroundID);
			}
		}
	}
}
