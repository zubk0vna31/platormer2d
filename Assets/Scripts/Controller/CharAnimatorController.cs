using System;
using System.Collections.Generic;
using UnityEngine;

namespace Platformer2D
{
    public class CharAnimatorController : IDisposable
    {
        private sealed class Animation
        {
            public AnimState track;
            public List<Sprite> sprites;
            public bool loop;
            public float speed;
            public float counter;
            public bool sleep;

            public void Update()
            {
                if (sleep)
                    return;
                counter += Time.deltaTime * speed;
                if (loop)
                {
                    while (counter >= sprites.Count)
                    {
                        counter -= sprites.Count;
                    }
                }
                else if (counter < sprites.Count)
                {
                    counter = sprites.Count;
                    sleep = true;
                }
            }
        }

        private CharAnimatorConfig _config;
        private Dictionary<SpriteRenderer, Animation> _activeAnimation = new Dictionary<SpriteRenderer, Animation>();

        public CharAnimatorController(CharAnimatorConfig config)
        {
            _config = config;
        }

        public void StartAnimation(SpriteRenderer spriteRenderer, AnimState track, bool loop, float speed)
        {
            if (_activeAnimation.TryGetValue(spriteRenderer, out var animation))
            {
                animation.sleep = false;
                animation.loop = loop;
                animation.speed = speed;
                if (animation.track != track)
                {
                    animation.track = track;
                    animation.sprites = _config.sequences.Find(sequence => sequence.track == track).sprites;
                    animation.counter = 0;
                }
            }
            else
            {
                _activeAnimation.Add(spriteRenderer, new Animation()
                {
                    loop = loop,
                    speed = speed,
                    track = track,
                    sprites = _config.sequences.Find(sequence => sequence.track == track).sprites
                });
            }
        }

        public void StopAnimation(SpriteRenderer spriteRenderer)
        {
            if (_activeAnimation.ContainsKey(spriteRenderer))
            {
                _activeAnimation.Remove(spriteRenderer);
            }
        }
        public void Update()
        {
            foreach (var animation in _activeAnimation)
            {
                animation.Value.Update();
                if (animation.Value.counter < animation.Value.sprites.Count)
                {
                    animation.Key.sprite = animation.Value.sprites[(int) animation.Value.counter];
                }
            }
        }
        public void Dispose()
        {
            _activeAnimation.Clear();
        }
    }
}
