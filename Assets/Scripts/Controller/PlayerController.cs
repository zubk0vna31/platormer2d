﻿using UnityEngine;

namespace Platformer2D
{
    public class PlayerController
    {
        private float _xAxisInput;
        private bool _isJump;
        private bool _isMoving;

        private float _speed = 3f;
        private float _animationSpeed = 10f;
        private float _jumpSpeed = 9f;
        private float _movingTreshhold = 0.1f;
        private float _jumpTreshhold = 1f;

        private float _g = -9.8f;
        private float _yVelocity;
        private float _groundLevel;

        private Vector3 _leftScale = new Vector3(-1, 1, 1);
        private Vector3 _rightScale = new Vector3(1, 1, 1);

        private LevelObjectView _view;
        private CharAnimatorController _animator;
        
        public PlayerController(LevelObjectView player, CharAnimatorController animator)
        {
            _view = player;
            _animator = animator;
            _animator.StartAnimation(_view.spriteRenderer, AnimState.Idle,true, _animationSpeed);
        }

        public bool IsGrounded()
        {
            return _view.transform.position.y <= _groundLevel + float.Epsilon && _yVelocity <= 0;
        }

        public void MoveTowards()
        {
            _view.transform.position += Vector3.right * Time.deltaTime * _speed * (_xAxisInput < 0 ? -1 : 1);
            _view.transform.localScale = (_xAxisInput < 0 ? _leftScale : _rightScale);
        }

        public void Update()
        {
            _animator.Update();
            _xAxisInput = Input.GetAxis("Horizontal");
            _isJump = Input.GetAxis("Vertical") > 0;
            _isMoving = Mathf.Abs(_xAxisInput) > _movingTreshhold;
            if (_isMoving)
            {
                MoveTowards();
            }

            if (IsGrounded())
            {
                _animator.StartAnimation(_view.spriteRenderer, _isMoving ? AnimState.Run : AnimState.Idle, true, _animationSpeed);
                if (_isJump && _yVelocity <= 0)
                {
                    _yVelocity = _jumpSpeed;
                }
                else if (_yVelocity < 0)
                {
                    _yVelocity = float.Epsilon;
                    _view.transform.position = _view.transform.position.Change(y: _groundLevel);
                }
            }
            else
            {
                _yVelocity += _g * Time.deltaTime;
                if (Mathf.Abs(_yVelocity) > _jumpTreshhold)
                {
                    _animator.StartAnimation(_view.spriteRenderer, AnimState.Jump, true, _animationSpeed);
                }

                _view.transform.position += Vector3.up * (_yVelocity * Time.deltaTime);
            }
            
        }
    }
}