using System;
using System.Collections.Generic;
using UnityEngine;

namespace Platformer2D
{
    public enum AnimState
    {
        Idle = 0,
        Run = 1,
        Jump = 2,
        Attack =3
    }

    [CreateAssetMenu(fileName = "AnimatorCfg", menuName = "Configs/ Char Animation Cfg", order = 1)]
    public class CharAnimatorConfig : ScriptableObject
    { 
        [Serializable]
        public sealed class SpriteSequence
        {
            public AnimState track;
            public List<Sprite> sprites = new List<Sprite>();
        }

        public List<SpriteSequence> sequences = new List<SpriteSequence>();
    }
}
