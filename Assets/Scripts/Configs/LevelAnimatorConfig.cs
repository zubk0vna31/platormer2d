﻿using System;
using UnityEngine;

namespace Platformer2D
{
    [CreateAssetMenu(fileName = "LevelAnimatorCfg", menuName = "Configs/ Level Animation Cfg", order = 2)]
    
    
    public class LevelAnimatorConfig: ScriptableObject
    {
            public float speed;
            [NonSerialized] public float destinationPoint = 30f;
            [NonSerialized] public float startPoint = 28f;
    }
    
}